let name = "John";
let age = 20;
let isActive = true; 
console.log("username:", name, "his age:", age, "is active", isActive);

let arr = [name, age, isActive];
console.log("this is array:", arr);

let arr1 = ["susy", 22, false, 35];
console.log("this is array for susy:", arr1);

let userJohn = {
	name: "john",
  age: 20,
  isActive: true
};
console.log("john age is:", userJohn.age);
let userSusy = {
	name: "susy",
  age: 22,
  address: {
  	city: "Moscow",
    street: "Litovskaya",
    house: 13
  },
  isActive: false
};

console.log("susy address street is:", userSusy.address.street);

//--------
//1 запись:
//имя: джон, возраст: 20, активность: тру
//имя: сюзи, возраст: 22, активность: фолс
let users = [{name: "john", age: 20}, {name:"susy", age: 22}];
console.log("all users:", users);
console.log("name of first user:", users[0].name);

// чтобы пройти по массиву данных, нам нужно использовать цикл

for (let i = 0; i < users.length; i++) {
	let user = users[i];
  console.log("current user name:", user.name);
}

let wallet = [123,10,15,50,20];

function printValue(num) {
	console.log("current value:", num);
}

for (let i = 0; i < wallet.length; i++) {
   printValue(wallet[i]);
//   console.log("current value:", wallet[i]);
};
}


let i = 3;
do {
	console.log("olala", i);
  i++;
} while(i < 2);

let n =3;
while(n < 2) {
	console.log("second", n);

}
console.log("finish");

if (n===3){
	console.log("n equal 3");
} else {
	console.log("else n == 4");

}

numStr = "55"
console.log(parseFloat(numStr, 10));